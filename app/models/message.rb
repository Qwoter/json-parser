class Message < ActiveRecord::Base
  attr_accessible :hashed_id, :n, :t, :parent_id, :lft, :rgt

  def title
    "id: #{self.hashed_id} | n: #{self.n} | t: #{self.t}"
  end

  include TheSortableTree::Scopes
  # acts_as_nested_set
end
