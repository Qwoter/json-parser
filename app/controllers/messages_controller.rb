class MessagesController < ApplicationController
  include TheSortableTreeController::Rebuild

  def manage
    @messages = Message.nested_set.select('id, parent_id, n, t, hashed_id').all
  end

  # any code here
end