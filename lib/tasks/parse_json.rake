#lib/tasks/parse_json.rake
require 'rake'
namespace :parse_json do
  task :parse => :environment do
    ParseJson.parse_json_to_db 
    puts "Success"
  end
end

task :parse_json => 'parse_json:parse'
