class ParseJson
  def self.tree(data, parent)
    data.each_with_index do |element, index|
      m = Message.create(hashed_id: element["id"], n: element["n"], t: element["t"], parent_id: parent)

      if index == 0
        m.lft = nil
        m.rgt = m.id + 1
      elsif index == data.size - 1
        m.lft = m.id - 1
        m.rgt = nil
      else
        m.lft = m.id - 1
        m.rgt = m.id + 1
      end

      m.save

      self.tree(element["objects"], m.id) if element["objects"]
    end
  end

  def self.parse_json_to_db
    file = File.open("lib/classes/hash.json", "r")
    contents = file.read
    file.close

    data = JSON.parse(contents)

    self.tree(data, nil)
    return
  end
end
