class CreateMessages < ActiveRecord::Migration
  def change
    create_table :messages do |t|
      
      t.string :hashed_id
      t.string :t
      t.string :n
      t.integer :parent_id
      t.integer :lft
      t.integer :rgt
    end
  end
end
